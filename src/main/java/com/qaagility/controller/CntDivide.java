package com.qaagility.controller;

public class CntDivide {

    public int divide(int numerator, int denominator) {
        if (denominator == 0) {
            return Integer.MAX_VALUE;
	}
        else {
            return numerator / denominator;
	}
    }

}
