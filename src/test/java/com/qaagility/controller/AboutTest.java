
package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class AboutTest {

        @Test
        public void testDesc() throws Exception {
                String output = new About().desc();
		assertTrue("AboutTest", output.contains("This application was copied from somewhere"));
        }
}

