
package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class CntTest {

        @Test
        public void testD() throws Exception {
                CntDivide cnt = new CntDivide();
		int output1 = cnt.divide(4,2);
		assertEquals("TestD1", 2, output1);
		int output2 = cnt.divide(5,0);
		assertTrue("TestD2", output2 > 0);
        }
}


